import React, { useState, useEffect } from 'react';
import Header from '../../components/Header';
import ListTask from './components/ListTask';
import AddTask from './components/AddTask';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { changeToken } from '../../../Store/actions/changeToken';

import '../../../scss/home.scss';

const Home = () => {
    const store = useSelector(globalState => globalState.globalState);
    const [tasks, settasks] = useState([]);
    const [isAdd, setisAdd] = useState(true);
    const [isUpdate, setIsUpdate] = useState(false);
    const [isRender, setIsRender] = useState(false);
    const [updateTask, setUpdateTask] = useState({});
    const dispatch = useDispatch();

    useEffect(() => {
        axios({
            method: 'post',
            url: 'http://localhost:1337/list-todo',
            headers: {
                token: store.infor.token
            }
        })
            .then(res => {
                settasks(res.data)
                if (res.data.token) {
                    dispatch(changeToken(res.data.token));
                }
            })
    }, [store.infor.token, dispatch, isRender])

    const callbackFunction = (childData) => {
        setIsRender(!isRender);
        // setisAdd(!isAdd);
    };

    const handleDeleteTask = (index) => {
        setIsRender(!isRender);
    }

    const closeAddTask = (status) => {
        setisAdd(status);
    }

    const handleUpdateTask = (task) => {
        setisAdd(true);
        setIsUpdate(true);
        setUpdateTask(task);
    }

    return (
        <div>
            <Header />
            <button
                className='btn-add-task'
                onClick={() => {
                    if (isUpdate === true) {
                        setIsUpdate(false);
                        setUpdateTask({});
                    } else {
                        closeAddTask(!isAdd);
                    }
                    
                }}
            >
                Add Task
            </button>
            <div style={{ display: 'flex', padding: '20px 50px' }}>
                {isAdd === true && store.login === true
                    ? <AddTask
                        status={isAdd}
                        parentCallback={callbackFunction}
                        callbackClose={closeAddTask}
                        update={isUpdate}
                        dataUpdate={updateTask}
                    />
                    : ''}
                {store.login === true
                    ? <ListTask
                        data={tasks}
                        status={isAdd}
                        handleDeleteTask={(index) => {
                            handleDeleteTask(index)
                        }}
                        handleUpdateTask={(task) => {
                            handleUpdateTask(task)
                        }}
                    />
                    : ''}
            </div>
        </div>
    )
}

export default Home