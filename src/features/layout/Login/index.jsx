import React, { useState, useRef } from 'react';
import TextField from '@mui/material/TextField';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import publicIp from 'react-public-ip';
import { useDispatch } from 'react-redux';

import { showSuccessToast } from '../../../utilities/showMessage';
import { login } from '../../../Store/actions/login';
import { userLogin } from '../../../Store/actions/userLogin';

import '../../../scss/toast.scss';
import '../../../scss/login.scss';

const Login = () => {

    const [username, setusername] = useState('');
    const [password, setpassword] = useState('');
    const [email, setemail] = useState('');
    // const [device, setdevice] = useState('')
    const [verifyCode, setverifyCode] = useState('');
    const navigate = useNavigate();
    const verifyForm = useRef();
    const loginForm = useRef();
    const dispatch = useDispatch();

    const handleSignIn = async () => {
        if (username !== '' && password !== '') {

            const useragent = navigator.userAgent;
            await localStorage.setItem('device', useragent);
            (async () => {
                return await publicIp.v4(); // for public ip v4
                // console.log(await publicIp.v6()); // for public ip v6   
            })()
                .then(async res => await localStorage.setItem('ip', res))

            const user = await {
                username: username,
                password: password,
                device: localStorage.getItem('device'),
                ip: localStorage.getItem('ip'),
            }

            await axios.post('http://localhost:1337/login', user)
                .then(function (res) {
                    const status = res.data;
                    if (status.message) {
                        showSuccessToast('Có lỗi xảy ra', status.message, 'error');
                        if (status.message === 'Chưa xác thực' || status.type) {
                            verifyForm.current.style.display = 'block';
                            loginForm.current.style.display = 'none';
                            setemail(status.type);
                        }
                    } else {
                        localStorage.setItem('user', status.user.name);
                        dispatch(login(true));
                        dispatch(userLogin(status));
                        showSuccessToast('Success', 'Đăng nhập thành công', 'success');
                        setTimeout(() => {
                            navigate('/home');
                        }, 2000);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                });
        }
    }

    const handleVerify = async () => {

        const user = {
            username: username,
            password: password,
            device: localStorage.getItem('device'),
            ip: localStorage.getItem('ip'),
            verifyCode: verifyCode
        }

        await axios.post('http://localhost:1337/login', user)
            .then(function (res) {
                const status = res.data;
                if (status.message) {
                    showSuccessToast('Có lỗi xảy ra', status.message, 'error');
                } else {
                    localStorage.setItem('user', status.user.name);
                    dispatch(login(true));
                    dispatch(userLogin(status));
                    showSuccessToast('Success', 'Đăng nhập thành công', 'success');
                    setTimeout(() => {
                        navigate('/home');
                    }, 2000);
                }
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    return (
        <div>
            <div id="toast"></div>
            <div className='login'>
                <div className="login-form" ref={loginForm}>
                    <p>Login</p>
                    <div className='form-input'>
                        <TextField
                            label="Username"
                            variant="outlined"
                            fullWidth
                            onChange={(e) => setusername(e.target.value)}
                            value={username}
                        />
                    </div>
                    <div className='form-input'>
                        <TextField
                            label="Password"
                            variant="outlined"
                            fullWidth
                            type="password"
                            onChange={(e) => setpassword(e.target.value)}
                            value={password}
                        />
                    </div>
                    <button onClick={handleSignIn} >Sign In</button>
                </div>
                <div className='login-form verify-form' ref={verifyForm}>
                    <p>Verify</p>
                    <span>Hệ thống đã gửi mã xác thực tới</span><br />
                    <span style={{ fontWeight: 'bold', fontSize: '20px', color: 'red' }}>{email}</span>
                    <div className='form-input' style={{ marginTop: '20px' }}>
                        <TextField
                            label="Mã xác thực"
                            variant="outlined"
                            fullWidth
                            onChange={(e) => setverifyCode(e.target.value)}
                            value={verifyCode}
                            type="number"
                        />
                    </div>
                    <button onClick={handleVerify}>Xác thực</button>
                </div>
            </div>
        </div>
    )
}

export default Login