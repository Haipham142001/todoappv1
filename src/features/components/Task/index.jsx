import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGear, faTrash } from '@fortawesome/free-solid-svg-icons'
import { API_DELETE_TODO } from '../../../constant/api';
import { useSelector, useDispatch } from 'react-redux';
import { showSuccessToast } from '../../../utilities/showMessage';
import { changeToken } from '../../../Store/actions/changeToken';
import { login } from '../../../Store/actions/login';
import { userLogin } from '../../../Store/actions/userLogin';
import { useNavigate } from 'react-router-dom';

const Task = (props) => {

    const task = props.data;
    const store = useSelector(globalState => globalState.globalState);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleDeleteTask = async (id) => {
        const response = await fetch(API_DELETE_TODO, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'token': store.infor.token
            },
            body: JSON.stringify({ id: id })
        })

        const value = await response.json();

        if (value.message === 'Xóa hoàn tất') {
            showSuccessToast('Thành công', value.message, 'success');
            props.onDelete()
            if (value.token) {
                dispatch(changeToken(value.token));
            }
        }
        if (value.message === 'Phiên đăng nhập hết hạn. Đăng nhập lại') {
            localStorage.clear();
            localStorage.clear();
            dispatch(login(false));
            dispatch(userLogin({}));
            showSuccessToast('Thành công', value.message, 'error');
            setTimeout(() => {
                navigate('/');
            }, 2000);
        }
    }

    const handleUpdateTask = (task) => {
        props.onUpdate(task);
    }

    return (
        <tr>
            <td>{task.name}</td>
            <td>{task.dateStart}</td>
            <td>{task.deadline}</td>
            <td>
                {task.status === true
                    ? <input type="checkbox" checked />
                    : <input type="checkbox" />}
            </td>
            <td>
                <button
                    onClick={() => handleUpdateTask(task)}>
                    <FontAwesomeIcon icon={faGear} />
                    Sửa</button>
                <button
                    onClick={() => handleDeleteTask(task.id)}>
                    <FontAwesomeIcon icon={faTrash} />
                    Xóa</button>
            </td>
        </tr>
    )
}

export default Task