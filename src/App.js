import { Routes, Route } from "react-router-dom";
import './App.css';
import Home from "./features/layout/Home";
import Login from "./features/layout/Login";

function App() {
  return (
    <Routes>
      <Route path="/" exact element={<Home />} />
      <Route path="/home" element={<Home />} />
      <Route path="/login" element={<Login />} />
    </Routes>
  );
}

export default App;
