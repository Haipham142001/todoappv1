const HOST = 'http://localhost:1337/';
export const API_CREATE_TODO = HOST + 'create-todo';
export const API_DELETE_TODO = HOST + 'delete-todo';
export const API_UPDATE_TODO = HOST + 'update-todo';
export const API_LIST_TODO = HOST + 'list-todo';