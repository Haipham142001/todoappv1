// import { useReducer } from "react";
import { CHANGETOKEN, LOGIN, USERLOGIN } from "../constants";

const initState = JSON.parse(localStorage.getItem('store')) || {
    login: false,
    infor: {}
}

const globalState = (state = initState, action) => {
    switch (action.type) {
        case LOGIN:
            initState.login = action.productInfo;
            localStorage.setItem('store', JSON.stringify(initState));
            return initState;
        case USERLOGIN:
            initState.infor = action.productInfo;
            localStorage.setItem('store', JSON.stringify(initState));
            return initState;
        case CHANGETOKEN:
            initState.infor.token = action.productInfo;
            localStorage.setItem('store', JSON.stringify(initState));
            return initState;
        default:
            return state;
    }
}

export default globalState;