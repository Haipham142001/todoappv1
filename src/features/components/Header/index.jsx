import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import axios from 'axios';

import '../../../scss/header.scss';
import { login } from '../../../Store/actions/login';
import { userLogin } from '../../../Store/actions/userLogin';
import { showSuccessToast } from '../../../utilities/showMessage';

const Header = () => {

    const value = useSelector(globalState => globalState.globalState);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogout = async () => {
        await axios({
            method: 'post',
            url: 'http://localhost:1337/logout',
            headers: {
                token: value.infor.token
            }
        }).then(function (res) {
            const status = res.data;

            if (status.message === 'Đăng xuất thành công') {
                localStorage.clear();
                dispatch(login(false));
                dispatch(userLogin({}));
                showSuccessToast('Success', 'Đăng xuất thành công', 'success');
                setTimeout(() => {
                    navigate('/');
                }, 2000);
            } else {
                showSuccessToast('Thất bại', status.message, 'error');
            }
        })
            .catch(function (err) {
                console.log(err);
            });
    }

    return (
        <div className='header'>
            <div id="toast"></div>
            <ul>
                <li>
                    <Link to="/home">
                        Home
                    </Link>
                </li>
                <li>
                    {value.login === true
                        ? <span
                            onClick={handleLogout}
                            style={{ cursor: 'pointer' }}
                        >
                            Đăng xuất
                        </span>
                        : <Link to="/login">
                            Login
                        </Link>
                    }
                </li>
            </ul>
        </div>
    )
}

export default Header