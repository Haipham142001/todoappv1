import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTimes, faGear } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux';
import { changeToken } from '../../../../Store/actions/changeToken';
import { login } from '../../../../Store/actions/login';
import { userLogin } from '../../../../Store/actions/userLogin';
import { showSuccessToast } from '../../../../utilities/showMessage';
import { API_CREATE_TODO, API_UPDATE_TODO } from '../../../../constant/api';
import { useNavigate } from 'react-router-dom';

const AddTask = (props) => {

    const [name, setname] = useState('');
    const [dateStart, setDateStart] = useState('');
    const [deadline, setDeadline] = useState('');
    const store = useSelector(globalState => globalState.globalState);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const urlAddTask = API_CREATE_TODO;
    const urlUpdateTask = API_UPDATE_TODO;

    const handleAddTask = async () => {
        if (name !== '' && dateStart !== '' && deadline !== '') {

            const dataForm = {
                "name": name,
                "dateStart": dateStart,
                "deadline": deadline,
                "status": false,
                "id": props.update === true ? props.dataUpdate.id : ""
            }

            try {
                const response = await fetch(props.update === true ? urlUpdateTask : urlAddTask, {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                        'Content-Type': 'application/json',
                        'token': store.infor.token
                    },
                    body: JSON.stringify(dataForm)
                });

                const status = await response.json();

                if (status.message.toLowerCase().lastIndexOf('hoàn tất') !== '-1') {
                    showSuccessToast('Thành công', status.message, 'success');
                    props.parentCallback('a');
                    setname('');
                    setDeadline('');
                    setDateStart('');
                    if (status.token) {
                        dispatch(changeToken(status.token));
                    }
                    if (props.update === true) {
                        props.callbackClose(false);
                    }
                }
                if (status.message === 'Phiên đăng nhập hết hạn. Đăng nhập lại') {
                    localStorage.clear();
                    dispatch(login(false));
                    dispatch(userLogin({}));
                    setTimeout(() => {
                        navigate('/');
                    }, 2000);
                }
            } catch (e) {
                alert('Lỗi hệ thống');
            }
        }
    }

    const handleCloseAddTask = () => {
        props.callbackClose(false);
    }

    useEffect(() => {
        if (props.status === true) {
            setname(props.dataUpdate.name);
            setDateStart(props.dataUpdate.dateStart);
            setDeadline(props.dataUpdate.deadline);
        }
    }, [props.status, props.dataUpdate])


    return (
        <div
            className={props.status === true
                ? 'add-task'
                : 'hidden'}>
            <h1><center>{props.update === true ? 'Update' : 'Add'}</center></h1>
            <div id="toast"></div>
            <input
                type="text"
                placeholder='Name'
                value={name || ''}
                onChange={(e) => setname(e.target.value)}
            />
            <input
                type="text"
                placeholder='Date start'
                value={dateStart || ''}
                onChange={(e) => setDateStart(e.target.value)}
            />
            <input
                type="text"
                placeholder='Deadline'
                value={deadline || ''}
                onChange={(e) => setDeadline(e.target.value)}
            />
            <button
                onClick={handleAddTask}>
                <FontAwesomeIcon icon={props.update === true ? faGear : faPlus} />
                {props.update === true ? 'Update' : 'Add'}
            </button>
            <div className='close-task'>
                <FontAwesomeIcon
                    icon={faTimes}
                    onClick={handleCloseAddTask}
                />
            </div>
        </div>
    )
}

export default AddTask