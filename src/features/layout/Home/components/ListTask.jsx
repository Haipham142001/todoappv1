import React, { memo } from 'react';
import Task from '../../../components/Task';
import '../../../../scss/task.scss';

const ListTask = (props) => {

    const data = props.data.todos;
    let list = <tr><td colSpan={5}><span>No task</span></td></tr>;

    if (Boolean(data) === true) {
        if (Object.keys(data).length === 0) {
            console.log('rỗng');
        } else {
            list = data.map((task, index) => {
                return <Task data={task} key={index} onDelete={() => { props.handleDeleteTask(index) }} onUpdate={() => { props.handleUpdateTask(task) }} />
            })
        }
    }

    return (
        <div className={props.status === true ? 'list-task add' : 'list-task'}>
            <table border={1}>
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Date start
                        </th>
                        <th>
                            Deadline
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {list}
                </tbody>
            </table>
        </div>
    )
}

export default memo(ListTask)